<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2021-2024 Teemu Ikonen -->
<!-- SPDX-License-Identifier: GPL-3.0-only -->
<component type="desktop-application">
  <id>page.codeberg.tpikonen.satellite</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0-only</project_license>
  <name>Satellite</name>
  <summary>Check your GPS reception and save your tracks</summary>
  <description>
    <p>Satellite displays global navigation satellite system (GNSS: that's GPS, Galileo, Glonass etc.) data obtained from an NMEA source in your device. Currently the ModemManager and gnss-share APIs are supported.</p>
    <p>You can use it to check the navigation satellite signal strength and see your speed, coordinates and other parameters once a fix is obtained. It can also save GPX-tracks.</p>
  </description>
  <developer id="tpikonen@mailbox.org">
    <name>Teemu Ikonen</name>
  </developer>
  <launchable type="desktop-id">page.codeberg.tpikonen.satellite.desktop</launchable>
  <url type="homepage">https://codeberg.org/tpikonen/satellite</url>
  <provides>
    <id>satellite.desktop</id>
    <binary>satellite</binary>
  </provides>
  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </recommends>
  <requires>
    <display_length side="shortest" compare="ge">360</display_length>
  </requires>
  <content_rating type="oars-1.1"/>
  <screenshots>
    <screenshot type="default" environment="gnome">
      <caption>Default view</caption>
      <image>https://tpikonen.codeberg.page/satellite/img/screenshot-default.png</image>
    </screenshot>
    <screenshot environment="gnome">
      <caption>Expanded view</caption>
      <image>https://tpikonen.codeberg.page/satellite/img/screenshot-expanded.png</image>
    </screenshot>
    <screenshot environment="gnome:dark">
      <caption>Landscape view, dark mode</caption>
      <image>https://tpikonen.codeberg.page/satellite/img/screenshot-landscape-dark.png</image>
    </screenshot>
  </screenshots>
  <releases>
    <release version="0.9.0" date="2024-11-26">
      <description>
        <p>The fancy release</p>
        <ul>
          <li>Port to GTK4 and libadwaita with the latest widgets</li>
          <li>Move the log container to the right side of the window when expanded</li>
          <li>Add a horizontal display mode for mobile devices in landscape mode</li>
          <li>Add 'Copy coordinates' (to clipboard) command to the app menu</li>
        </ul>
      </description>
    </release>
    <release version="0.5.0" date="2024-09-23">
      <description>
        <p>The robust release</p>
        <ul>
          <li>Make the mm source more robust by resetting ModemManager props with notifier callbacks</li>
          <li>Add an experimental gpsd source</li>
          <li>Select source quirks with a new CLI option '-q' / '--quirks'</li>
          <li>Display GPX track duration and distance when recording</li>
          <li>Add 'type' and 'path' attributes to sources, log them on startup</li>
          <li>Do not enable AGPS on a non-enabled (SIMless) modem in mm source</li>
        </ul>
      </description>
    </release>
    <release version="0.4.3" date="2024-03-23">
      <description>
        <p>The SIMless release</p>
        <ul>
          <li>Support NMEA GLL sentences</li>
          <li>ModemManager source is now slightly more robust against interference from other apps</li>
          <li>ModemManager source now finds a modem with GPS capability, even if it's not first one</li>
          <li>ModemManager source does not require the modem to be in 'enabled' state. This allows modems which are locked, or even without a SIM card to be used as GPS sources (requires ModemManager version >= 1.23.2)</li>
        </ul>
      </description>
    </release>
    <release version="0.4.2" date="2023-09-23">
      <description>
        <p>The geoidal release</p>
        <ul>
          <li>Add 'Geoidal separation' field to dataframe</li>
          <li>Display DOPs (PDOP, HDOP, VDOP) on a single dataframe line</li>
          <li>Various small fixes to gnss-share source, logging, NMEA parsing etc.</li>
        </ul>
      </description>
    </release>
    <release version="0.4.1" date="2023-05-26">
      <description>
        <p>The automatic release</p>
        <ul>
          <li>Autodetect sources and source quirks when --source option is not given</li>
          <li>Some small fixes to mm_glib_source, NMEA parsing, flatpak, etc.</li>
        </ul>
      </description>
    </release>
    <release version="0.4.0" date="2023-03-22">
      <description>
        <p>The managerial release</p>
        <ul>
          <li>Use mm-glib to talk to ModemManager, remove pydbus</li>
          <li>Support 'quirks' in the ModemManager source, e.g. Quectel talker fixes</li>
          <li>Various reliability fixes</li>
        </ul>
      </description>
    </release>
    <release version="0.3.1" date="2022-11-17">
      <description>
        <p>The quickfix release </p>
        <ul>
          <li>Fix screenshot links, so that flathub builds work</li>
        </ul>
      </description>
    </release>
    <release version="0.3.0" date="2022-11-17">
      <description>
        <p>The multiplatform release</p>
        <ul>
          <li>Add UnixNmeaSource and GnssShareNmeaSource, enabling support for devices which use gnss-share, like Librem 5 (thanks devrtz)</li>
          <li>Allow specifying NmeaSource from the command line (thanks devrtz)</li>
          <li>Prefilter NMEA sentences before parsing, enabling support for devices which emit proprietary sentences, like Oneplus 6</li>
          <li>Display app menu on edge-overshot only on touchscreen devices</li>
          <li>Flatpak: Update to Gnome runtime 43 (thanks ferenc)</li>
        </ul>
      </description>
    </release>
    <release version="0.2.8" date="2022-09-07">
      <description>
        <p>Ageless no more</p>
        <ul>
          <li>Fix the fallback timeout, so that the 'Age of update' field is always updated</li>
          <li>Small bugfixes, cleanups and some internal refactoring</li>
        </ul>
      </description>
    </release>
    <release version="0.2.7" date="2022-08-10">
      <description>
        <p>Now with less chatter</p>
        <ul>
          <li>Print signal output to stdout only with command line argument '-c'</li>
          <li>Add a pulley menu imitation: Display main menu on data window upper edge overshot</li>
          <li>Make the main popup-menu buttons larger</li>
          <li>Update Gnome runtime to version 42, GPXPy to version 1.5.0</li>
        </ul>
      </description>
    </release>
    <release version="0.2.6" date="2021-11-19">
      <description>
        <p>Satellite without satellites</p>
        <ul>
          <li>Allow running without a working NMEA source</li>
          <li>Improve appdata and screenshots</li>
        </ul>
      </description>
    </release>
    <release version="0.2.5" date="2021-11-18">
      <description>
        <p>Now with more flat</p>
        <ul>
          <li>Save GPX tracks to ~/Documents/satellite-tracks, restrict flatpak filesystem permissions to this dir</li>
        </ul>
      </description>
    </release>
    <release version="0.2.4" date="2021-11-17">
      <description>
        <p>Coming soon to a hub near you</p>
        <ul>
          <li>Change app-id to page.codeberg.tpikonen.satellite</li>
          <li>Improve resilience with disappearing and reappearing modem</li>
          <li>Show the age of last location update, in addition to age of fix</li>
          <li>Various flatpak manifest and appdata improvements required for submitting Satellite to flathub</li>
        </ul>
      </description>
    </release>
    <release version="0.2.3" date="2021-10-25">
      <description>
        <p>The monthly release</p>
        <ul>
          <li>Tap / click on the satellite SNR barchart to expand or contract it</li>
          <li>Print a unicode arrow showing the approximate bearing next to the speed display and the 'True Course' data row</li>
        </ul>
      </description>
    </release>
    <release version="0.2.2" date="2021-09-21">
      <description>
        <p>Now with assistance</p>
        <ul>
          <li>Use assisted GPS (AGPS MSB) if supported</li>
          <li>Log modem info on startup, check XTRA assistance data validity on Quectel modems (i.e. the Pinephone)</li>
          <li>Restore previous GPS settings when quitting</li>
          <li>Support NMEA sentences from all constellations known to Quectel EG25 (Pinephone): GPS, Glonass, Galileo, Beidou and QZSS</li>
          <li>Make satellites from Galileo, Beidou and QZSS have PRNs with prefixes (E, C and J)</li>
          <li>Add 'Receiving sats' line to dataframe</li>
        </ul>
      </description>
    </release>
    <release version="0.2.1" date="2021-08-31">
      <description>
        <p>Now with more satellites</p>
        <ul>
          <li>Inhibit system suspend while recording a GPX track</li>
          <li>Show info from Galileo and Beidou satellites correctly</li>
          <li>Reorganize data frame rows to better show multi-constellation data</li>
        </ul>
      </description>
    </release>
    <release version="0.2.0" date="2021-08-20">
      <description>
        <p>Now with GPX support</p>
        <ul>
          <li>Record GPX tracks</li>
          <li>Ignore 'Modem not found' errors for a while</li>
        </ul>
      </description>
    </release>
    <release version="0.1.1" date="2021-08-01">
      <description>
        <p>First (working) release</p>
        <p>Release satellite with Python (pip) and flatpak build files.</p>
      </description>
    </release>
    <release version="0.1.0" date="2021-07-30">
      <description>
        <p>First release!</p>
        <p>Satellite is now somewhat useful, so release it into the world.</p>
      </description>
    </release>
  </releases>
</component>
