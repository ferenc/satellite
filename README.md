# Satellite

![Main view](https://tpikonen.codeberg.page/satellite/img/screenshot-default.png)
![Expanded view](https://tpikonen.codeberg.page/satellite/img/screenshot-expanded.png)
![Landscape view, dark mode](https://tpikonen.codeberg.page/satellite/img/screenshot-landscape-dark.png)

Satellite displays global navigation satellite system (GNSS: GPS et al.) data
obtained from an [NMEA](https://gpsd.gitlab.io/gpsd/NMEA.html#_gns_fix_data)
source in your Linux device. It can read data from
[ModemManager](https://www.freedesktop.org/wiki/Software/ModemManager/),
[gnss-share](https://gitlab.com/postmarketOS/gnss-share) and
[gpsd](https://gpsd.gitlab.io/gpsd/) APIs. It can also save a track of your
movement to a GPX-file.

## License

GPL-3.0

## Dependencies:

    python 3.6+, PyGObject, GTK4, libadwaita, libmm-glib, pynmea2, gpxpy

## Installing and running

### Flathub

<a href='https://flathub.org/apps/details/page.codeberg.tpikonen.satellite'>
<img width='240' alt='Get it on Flathub' src='https://flathub.org/api/badge?locale=en'/>
</a>

Satellite is
[in flathub](https://flathub.org/apps/details/page.codeberg.tpikonen.satellite)
and can be installed from there, or from a software manager like Gnome software.
The direct install link is
[here](https://dl.flathub.org/repo/appstream/page.codeberg.tpikonen.satellite.flatpakref).

Run

    flatpak run page.codeberg.tpikonen.satellite

to execute from the command line.

### From source tree

Run the script `bin/satellite`.

### pip / pipx install from source tree

Run

    pip install --user --break-system-packages ./

in the source tree root.

This creates an executable Python script in `$HOME/.local/bin/satellite`.

Alternatively you can install to a venv with `pipx` by running these commands
in the source tree root:

    pipx install ./
    pipx inject -r requirements.txt satellite

### Flatpak from source tree

Run

    flatpak-builder --install --user build-dir flatpak/page.codeberg.tpikonen.satellite.yaml

in the source tree root to install a local build to the user flatpak repo.

## Hints

You can start recording a GPX track by selecting 'Record track' from the main
menu. The GPX file is saved in `$HOME/Documents/satellite-tracks`.
